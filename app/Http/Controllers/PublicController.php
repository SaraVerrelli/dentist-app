<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\AppointmentRequest;
use App\Http\Requests\ReviewRequest;
use App\Models\Review;
use GuzzleHttp\Middleware;

class PublicController extends Controller
{

    public function home()
    {
        $reviews = Review::all();

        return view('home', compact('reviews'));
    }

    public function appointment()
    {
        return view('appointment');
    }

    public function requestAppointment(AppointmentRequest $request)
    {



        $nome = $request->input('nome');
        $email = $request->input('email');
        $date = $request->input('date');
        $service = $request->input('service');
        $comment = $request->input('comment');
        $contact = compact('date', 'nome', 'comment');

        Mail::to($email)->send(new ContactMail($contact));



        return redirect(route('home'))->with('message', 'Grazie per averci contattato. Abbiamo preso in carica la tua richiesta, ti ricontatteremo appena possibile.');
    }

    public function review()
    {
        return view('review');
    }

    public function leaveReview(ReviewRequest $request)

    {
        //Metodo Mass Assignment
        $review = Review::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'service' => $request->input('service'),
            'comment' => $request->input('comment'),
            'image' => $request->file('image')->store('public/img'),
        ]);


        return redirect(route('home'))->with('message', 'Grazie per aver inviato una recensione.');
    }
}
