<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required|email:rfc,dns',
            'date' => 'required|date|after:today',
            'service' => 'required',
            'comment' => 'max:100',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Inserire un indirizzo email valido dove possiamo contattarla.',
        ];
    }
}
