<x-layout>
    <div class="container-fluid">
        <div class="row mx-auto">
            <div class="col-12 col-md-6 my-auto">
                <img class="img-fluid" src="./media/calendar.png" alt="">
            </div>
           
            <div class="col-12 col-md-6 mt-3">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form method="POST" action="{{route('request.appointment')}}"> 
                    @csrf
                    <div class="mb-3">
                        <label for="InputName" class="form-label">Nome e cognome</label>
                        <input type="text" id="InputName" class="form-control" name="nome" value="{{old("nome")}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputEmail" class="form-label">Email</label>
                        <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" name="email" value="{{old("email")}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputDate" class="form-label">Scegli il giorno</label>
                        <input type="date" class="form-control" id="InputDate" name="date" value="{{old("date")}}">
                        
                    </div>
                    <div class="mb-3">
                        <label for="InputServizi">Servizi</label>
                        <select class="form-select" aria-label="Default select example" name="service" value="{{old("service")}}">
                            <option id="InputServizi" selected>Scegli un servizio</option>
                            <option value="1">Prima visita</option>
                            <option value="2">Visita di controllo</option>
                            <option value="3">Terapia</option>
                        </select>
                        
                    </div> 
                    <div class="mb-3">
                        <label for="InputComment" class="form-label">Note</label>
                        <input type="text" class="form-control" id="InputComment" name="comment" value="{{old("comment")}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Richiedi appuntamento</button>
                </form>
            </div>
        </div>
    </div>
    
    
</x-layout>