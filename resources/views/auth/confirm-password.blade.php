<x-layout>
    <div class="container-fluid mt-5 bg-light">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="row mx-auto justify-content-center wide">
            <div class="col-6">
                <form action="{{route('password.confirm')}}" method="POST" class="mt-5">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Conferma la tua password</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Conferma</button>

                </form>
            </div>
            
            
            
        </div>
    </div>
</x-layout>