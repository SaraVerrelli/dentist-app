<div class="container bg-white">
    <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
      <div class="col-md-4 d-flex align-items-center text-muted">© 2021 Company, Inc</div>
  
      <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
        <li class="ms-3"><a class="text-muted" href="#"><i class="fab fa-2x fa-twitter"></i></a></li>
        <li class="ms-3"><a class="text-muted" href="#"><i class="fab fa-2x fa-instagram"></i></a></li>
        <li class="ms-3"><a class="text-muted" href="#"><i class="fab fa-2x fa-facebook"></i></a></li>
      </ul>
    </footer>
  </div>