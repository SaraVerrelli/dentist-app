<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
    
    <title>Dentista</title>
  </head>
  <body>
      <x-navbar/>
      <header>
          <h1 class="text-center text-celeste">STUDIO DENTISTICO DENTISANI</h1>
      </header>
      {{$slot}}

      <x-footer/>

    <script src="https://kit.fontawesome.com/2bab0aa439.js" crossorigin="anonymous"></script>
    <script src="{{asset('js/app.js')}}"></script>

  </body>
</html>