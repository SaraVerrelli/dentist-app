
<nav class="navbar navbar-expand-md navbar-light bg-light">
  <div class="container-fluid">
    
    <a class="navbar-brand text-celeste" href="{{route('home')}}">DentiSani</a>
    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav mx-auto">
        <a class="nav-link mx-3" href="{{route('appointment')}}"><i class="fas fa-calendar-day"></i> Fissa un appuntamento</a>
        <div class="dropdown">
          <a class="nav-link mx-3 dropdown-toggle" href="#" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-tooth"></i></i> I nostri servizi</a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="#">Service one</a></li>
            <li><a class="dropdown-item" href="#">Service two</a></li>
            <li><a class="dropdown-item" href="#">Service three</a></li>
            <li><a class="dropdown-item" href="#">Service four</a></li>
            
          </ul>
          
        </div>
        
        <a class="nav-link mx-3" href="#"><i class="far fa-user-circle"></i></i> Il nostro staff</a>
        <a class="nav-link mx-3" href="{{route('review')}}"><i class="far fa-edit"></i> Lascia una recensione</a>
      </div>
    </div>
    @auth
    Ciao, {{Auth::user()->name}}
    
    @endauth
    
    
  </div>
  
</nav>
<div class="container-fluid">
  <div class="row">
    <div class="col-12 d-flex justify-content-end">
      @guest
      <a class="text-info mx-2 no-tx-deco tx-bold" href="{{route('register')}}">Registrati</a>
      <a class="text-info mx-2 no-tx-deco tx-bold" href="{{route('login')}}">Login</a>  
      @endguest
      @auth
      
      <a class="text-info mx-2 no-tx-deco tx-bold" href="{{route('logout')}}" onclick="event.preventDefault();
      document.getElementById('form-logout').submit();">Logout</a>
      <form method="POST" action="{{route('logout')}}" id="form-logout">
        @csrf
      </form> 
      @endauth
    </div>
  </div>
  
</div>