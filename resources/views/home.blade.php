<x-layout>
    @if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
    @endif
    <main class="mt-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <img class="img-fluid" src="./media/dentist.jpg" alt="">
                </div>
                <div class="col-12 col-sm-6">
                    <h2>News</h2>
                    <hr id="hr-under-news">
                    <ul class="list-unstyled">
                        <li>
                            <a class="news-href" href="">
                                <h5>titolo</h5>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                            </a>
                        </li>
                        <li>
                            <a class="news-href" href="">
                                <h5>titolo</h5>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                            </a>
                        </li>
                        <li>
                            <a class="news-href" href="">
                                <h5>titolo</h5>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid mt-3">
            <div class="row">
                <h3>Dicono di noi</h3>
                @foreach ($reviews as $review)
                <div class="col-12 col-sm-6 col-md-4 mb-3">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4">
                                @if ($review->image)
                                <img src="{{Storage::url($review->image)}}" class="img-fluid rounded-start" alt="...">

                                @else
                                <img src="./media/blank-profile.png" class="img-fluid rounded-start" alt="...">

                                @endif
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{$review->name}}</h5>
                                    <p class="card-text">{{$review->comment}}</p>
                                    <p class="card-text"><small class="text-muted">{{$review->service}}</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                @endforeach
                
            </div>
        </div>
    </main>
</x-layout>