<x-layout>
    <div class="container-fluid">
        <div class="row mx-auto justify-content-center">
            
            
            <div class="col-12 col-md-6 mt-3">
            <h3 class="mb-3">Lasciaci una recensione</h3> 
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif  
                <form method="POST" action="{{route('leave.review')}}" enctype="multipart/form-data"> 
                    @csrf
                    <div class="mb-4">
                        <select name="stars">
                            <option value="1"><i class="star fa fa-star fa-3x mx-2"></i></option>
                            <option value="2">Dog</option>
                            <option value="3">Cat</option>
                            <option value="4">Hamster</option>
                            <option value="5">Parrot</option>
                        </select>
                        <form>
                            <input type="radio" id="1" name="fav_language" value="HTML">
                            <label for="html">HTML</label><br>
                            <input type="radio" id="css" name="fav_language" value="CSS">
                            <label for="css">CSS</label><br>
                            <input type="radio" id="javascript" name="fav_language" value="JavaScript">
                            <label for="javascript">JavaScript</label>
                        </form>

                        <input type=>
                       
                        <span id="star1" class="star fa fa-star fa-3x mx-2"></span>
                        <span id="star2" class="star fa fa-star fa-3x mx-2"></span>
                        <span id="star3" class="star fa fa-star fa-3x mx-2"></span>
                        <span id="star4" class="star fa fa-star fa-3x mx-2"></span>
                        <span id="star5" class="star fa fa-star fa-3x mx-2"></span>
                       
                    </div>
                    <div class="mb-3">
                        <label for="InputComment" class="form-label">Recensione</label>
                        <input type="text" class="form-control" id="InputComment" name="comment" value="{{old("comment")}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputServizi">Che servizio hai ricevuto?</label>
                        <select class="form-select" aria-label="Default select example" name="service" value="{{old("service")}}">
                            <option id="InputServizi" selected>Nessuno</option>
                            <option value="1">Servizio one</option>
                            <option value="2">Servizio two</option>
                            <option value="3">Servizio three</option>
                            <option value="4">Servizio four</option>

                        </select>
                        
                    </div> 
                    <div class="mb-3">
                        <label for="InputImmagine" class="form-label">Carica immagini</label>
                        <input type="file" class="form-control" id="InputImmagine" name="image">
                    </div>
                    <div class="mb-3">
                        <label for="InputName" class="form-label">Nome</label>
                        <input type="text" id="InputName" class="form-control" name="name" value="{{old("name")}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputEmail" class="form-label">Email</label>
                        <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" name="email" value="{{old("email")}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Invia recensione</button>
                </form>
            </div>
        </div>
    </div>
    
</x-layout>