<?php

use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/appointment', [PublicController::class, 'appointment'])->name('appointment');
Route::post('/appointment/book', [PublicController::class, 'requestAppointment'])->middleware(['password.confirm'])->name('request.appointment');
Route::get('/review', [PublicController::class, 'review'])->name('review');
Route::post('/review/leave', [PublicController::class, 'leaveReview'])->name('leave.review');
